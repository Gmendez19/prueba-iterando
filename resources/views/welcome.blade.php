<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Test Iterando</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

        <style>
            .overflow-3-l{
                overflow: hidden;
                text-overflow: ellipsis;
                -webkit-box-orient: vertical;
                word-break: break-all;
                display: -webkit-box;
                -webkit-line-clamp: 3;
            }
            .overflow-1-l{
                overflow: hidden;
                text-overflow: ellipsis;
                -webkit-box-orient: vertical;
                word-break: break-all;
                display: -webkit-box;
                -webkit-line-clamp: 1;
            }
        </style>
        
    </head>
    <body>
        <div class="container mt-4">
            <div class="row">
                @foreach ($users as $item)
                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-3">
                        <div class="card mb-4">
                            <div class="card-body d-flex flex-xl-column justify-content-xl-center align-items-xl-center flex-lg-column justify-content-lg-center align-items-lg-center flex-md-row justify-content-md-center align-items-md-baseline flex-sm-row justify-content-sm-center align-items-sm-baseline">
                                <div class="text-center">
                                    <img class="rounded-circle mt-2" src="{{ asset('images/perfil.png') }}" alt="Card image cap" style="height:50px; width:50px;">
                                </div>
                                <div class="text-center px-3">
                                    <h5 class="card-title overflow-1-l">{{$item->name}}</h5>
                                    <p class="card-text overflow-3-l ">Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente adipisci nisi, architecto perspiciatis consequatur repellendus excepturi non recusandae reprehenderit suscipit saepe molestiae. Sit ipsam eveniet nihil repellat? Sint, fugiat natus!</p>
                                </div>
                            </div>
                        </div>
                    </div>    
                @endforeach
            </div>    
        </div>


        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

        <!-- Popper JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    </body>
</html>
