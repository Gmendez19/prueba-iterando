<?php

namespace App\Http\Controllers\Api;

use App\Models\Event;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserHasEvents;
use App\Traits\ResponsesTrait;
use App\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use JWTAuth;

class EventController extends Controller
{
    //

    public function getAllEvents()
    {
        $data = Event::all();
        if ($data)
            return ResponsesTrait::responseSuccess(200, 'Lista de eventos', $data);
    }

    public function createEvent(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'idUser' => 'required',
            'name' => 'required|max:10000',
            'imageUrl' => 'required|file|max:2048',
            'dateTimeStart' => 'required',
            'description' => 'required|max:1000',
            'startDirection' => 'required|max:1000',
            'startLatitude' => 'required',
            'startLongitude' => 'required',
            'finishDirection' => 'required|max:1000',
            'finishLatitude' => 'required',
            'finishLongitude' => 'required',
        ]);

        if ($validator->fails()) {
            return ResponsesTrait::responseFails(400, "Error en la solicitud", $validator->errors()->all());
        } else {
            $event = new Event();
            $event->idUser = $request->input('idUser');
            $event->name = $request->input('name');
            $event->dateTimeStart = $request->input('dateTimeStart');
            $event->description = $request->input('description');
            $event->startDirection = $request->input('startDirection');
            $event->startLatitude = $request->input('startLatitude');
            $event->startLongitude = $request->input('startLongitude');
            $event->finishDirection = $request->input('finishDirection');
            $event->finishLatitude = $request->input('finishLatitude');
            $event->finishLongitude = $request->input('finishLongitude');
            if ($request->hasFile('imageUrl')) {
                $file = $request->file('imageUrl');
                $ext = md5(microtime()) . '.' . $file->getClientOriginalExtension();
                $path = Storage::disk('public')->putFileAs(
                    'events',
                    $file,
                    $ext
                );
                $event->imageUrl = env('APP_URL') . '/storage/' . $path;
            }
            $event->save();
            return ResponsesTrait::responseSuccess(201, "Evento creado correctamente", $event);
        }
    }

    public function detailEvent($idEvent)
    {
        $data = Event::find($idEvent);
        if ($data) {
            $object = new \stdClass();
            $user = User::find($data->idUser);
            $invitees = UserHasEvents::join('users',  'user_has_event.idUser','=', 'users.id')
            ->select('users.id', 'users.name','users.email', 'users.userRank')
            ->where('user_has_event.idEvent', '=', $idEvent )
            ->get();
            $object->propietario = $user;
            $object->invitados = $invitees;
            $object->evento = $data;
            return ResponsesTrait::responseSuccess(200, 'Lista de eventos', $object);
        } else {
            return ResponsesTrait::responseFails(400, "El evento no existe", []);
        }
    }

    public function updateEvent(Request $request, $idEvent)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:10000',
            'imageUrl' => 'required',
            'dateTimeStart' => 'required',
            'description' => 'required|max:1000',
            'startDirection' => 'required|max:1000',
            'startLatitude' => 'required',
            'startLongitude' => 'required',
            'finishDirection' => 'required|max:1000',
            'finishLatitude' => 'required',
            'finishLongitude' => 'required',
        ]);

        if ($validator->fails()) {
            return ResponsesTrait::responseFails(400, "Error en la solicitud", $validator->errors()->all());
        } else {
            $event = Event::find($idEvent);
            if ($event) {
                $event->name = $request->input('name');
                $event->dateTimeStart = $request->input('dateTimeStart');
                $event->description = $request->input('description');
                $event->startDirection = $request->input('startDirection');
                $event->startLatitude = $request->input('startLatitude');
                $event->startLongitude = $request->input('startLongitude');
                $event->finishDirection = $request->input('finishDirection');
                $event->finishLatitude = $request->input('finishLatitude');
                $event->finishLongitude = $request->input('finishLongitude');
                if ($request->hasFile('imageUrl')) {
                    $file = $request->file('imageUrl');
                    $ext = md5(microtime()) . '.' . $file->getClientOriginalExtension();
                    $path = Storage::disk('public')->putFileAs(
                        'events',
                        $file,
                        $ext
                    );
                    $event->imageUrl = env('APP_URL') . '/storage/' . $path;
                } else {
                    $event->imageUrl = $request->input('imageUrl');
                }
                $event->update();
                return ResponsesTrait::responseSuccess(201, "Evento actualizado correctamente", $event);
            }
            else{
                return ResponsesTrait::responseFails(401, "El evento no existe o pertenece a otro usuario", $validator->errors()->all());
            }
            
        }
    }
}
