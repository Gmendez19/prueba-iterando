<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\ResponsesTrait;
use App\User;

class UserController extends Controller
{
    //
    public function getUsers(){
        $data = User::all(); 
        if( $data)
            return ResponsesTrait::responseSuccess(200, 'Lista de usuarios', $data);
    }
}
