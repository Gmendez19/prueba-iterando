<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserHasEvents extends Model
{
    //
    protected $table =  'user_has_event';

    protected $fillable = [
        'idUser',
        'idEvent',
        'confirmedAttendance'
    ];
}
