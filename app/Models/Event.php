<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //
    protected $table =  'events';

    protected $fillable = [
        'id',
        'idUser',
        'name',
        'imageUrl',
        'dateTimeStart',
        'description',
        'startDirection',
        'startLatitude',
        'startLongitude',
        'finishDirection',
        'finishLatitude',
        'finishLongitude'
    ];

}
