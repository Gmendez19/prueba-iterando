<?php

use Illuminate\Database\Seeder;

class EventsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = \Faker\Factory::create();
        $total = 15;

        for ($i = 0; $i < $total; $i++) :
            $temp = $i + 1;
            $start = $faker->dateTimeBetween($startDate = '-15 days', $endDate = '+15 days', $timezone = null);
            \DB::table('events')->insert([
                'idUser' => $temp,
                'name' => $faker->sentence(5),
                'imageUrl' => $faker->imageUrl(640, 480),
                'dateTimeStart' => $start,
                'description' => $faker->text,
                'startDirection' => $faker->address,
                'startLatitude' => $faker->latitude(19, 20),
                'startLongitude' => $faker->longitude(-100, -99),
                'finishDirection' => $faker->address,
                'finishLatitude' => $faker->latitude(19, 20),
                'finishLongitude' => $faker->longitude(-100, -99),
            ]);
        endfor;
    }
}
