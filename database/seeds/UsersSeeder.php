<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = \Faker\Factory::create();
        $total = 15;

        for ($i = 0; $i < $total; $i++) :
            $temp = $i + 1;
            \DB::table('users')->insert([
                'name' => $faker->name(),
                'email' => 'test_' . $temp . '@iterando.mx',
                'password' => Hash::make('IterandoPrueba_' . $temp),
                'userRank' => $faker->numberBetween($min = 0, $max = 3)
            ]);
        endfor;
    }
}
