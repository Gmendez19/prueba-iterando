<?php

use Illuminate\Database\Seeder;

class UserHasEvents extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $total = 15;

        \DB::table('user_has_event')->insert(
            [
                [
                    'idUser' => 2,
                    'idEvent' => 1,
                    'confirmedAttendance' => rand(0, 1),
                ],
                [
                    'idUser' => 3,
                    'idEvent' => 1,
                    'confirmedAttendance' => rand(0, 1),
                ],
                [
                    'idUser' => 7,
                    'idEvent' => 1,
                    'confirmedAttendance' => rand(0, 1),
                ],
                [
                    'idUser' => 10,
                    'idEvent' => 1,
                    'confirmedAttendance' => rand(0, 1),
                ],
                [
                    'idUser' => 1,
                    'idEvent' => 2,
                    'confirmedAttendance' => rand(0, 1),
                ],
                [
                    'idUser' => 5,
                    'idEvent' => 2,
                    'confirmedAttendance' => rand(0, 1),
                ],
                [
                    'idUser' => 9,
                    'idEvent' => 2,
                    'confirmedAttendance' => rand(0, 1),
                ],
                [
                    'idUser' => 14,
                    'idEvent' => 3,
                    'confirmedAttendance' => rand(0, 1),
                ],
                [
                    'idUser' => 4,
                    'idEvent' => 3,
                    'confirmedAttendance' => rand(0, 1),
                ],
                [
                    'idUser' => 13,
                    'idEvent' => 5,
                    'confirmedAttendance' => rand(0, 1),
                ],
                [
                    'idUser' => 8,
                    'idEvent' => 5,
                    'confirmedAttendance' => rand(0, 1),
                ],
                [
                    'idUser' => 15,
                    'idEvent' => 5,
                    'confirmedAttendance' => rand(0, 1),
                ],
                [
                    'idUser' => 2,
                    'idEvent' => 6,
                    'confirmedAttendance' => rand(0, 1),
                ],
                [
                    'idUser' => 4,
                    'idEvent' => 6,
                    'confirmedAttendance' => rand(0, 1),
                ],
                [
                    'idUser' => 5,
                    'idEvent' => 6,
                    'confirmedAttendance' => rand(0, 1),
                ],
                [
                    'idUser' => 3,
                    'idEvent' => 7,
                    'confirmedAttendance' => rand(0, 1),
                ],
                [
                    'idUser' => 12,
                    'idEvent' => 7,
                    'confirmedAttendance' => rand(0, 1),
                ],
                [
                    'idUser' => 14,
                    'idEvent' => 7,
                    'confirmedAttendance' => rand(0, 1),
                ]
            ]
        );
    }
}
