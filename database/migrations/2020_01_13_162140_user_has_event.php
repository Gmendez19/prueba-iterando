<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserHasEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('user_has_event', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('idUser');
            $table->unsignedInteger('idEvent');
            $table->boolean('confirmedAttendance');
            $table->timestamps();

            $table->index(["idUser"], 'fk_user_has_events_user_idx');

            $table->foreign('idUser', 'fk_user_has_events_user_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->index(["idEvent"], 'fk_user_has_events_event_idx');

            $table->foreign('idEvent', 'fk_user_has_events_event_idx')
                ->references('id')->on('events')
                ->onDelete('no action')
                ->onUpdate('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
