<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('idUser');
            $table->string('name', 80)->nullable();
            $table->string('imageUrl', 200)->nullable();
            $table->datetime('dateTimeStart')->nullable();
            $table->text('description')->nullable();
            $table->string('startDirection', 150)->nullable();
            $table->decimal('startLatitude', 10, 6)->nullable();
            $table->decimal('startLongitude', 10, 6)->nullable();
            $table->string('finishDirection', 150)->nullable();
            $table->decimal('finishLatitude', 10, 6)->nullable();
            $table->decimal('finishLongitude', 10, 6)->nullable();
            $table->timestamps();

            $table->index(["idUser"], 'fk_event_has_user_idx');

            $table->foreign('idUser', 'fk_event_has_user_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
